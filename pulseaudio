#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

. "${TESTDIR}/common/update-test-path"

if [ $# -ne 1 ] || { [ "$1" != normal ] && [ "$1" != malicious ] ; }
then
    echo "Usage: $0 <normal|malicious>"
    exit 1
fi

test_pulseaudio() {
    set -e
    echo ">> 1"
    pactl stat
    pulseaudio --check
    echo ">> 2"
    pactl set-sink-volume 0 0
    echo ">> 3"
    paplay "${TESTDIR}/resources/media/audio/generic.wav"
    echo ">> 4"
    pactl set-sink-volume 0 50%
    echo ">> 5"
    pulseaudio --kill
    # Give pulseaudio time to quit
    sleep 3
    echo ">> 6"
    systemctl --user restart pulseaudio
    echo ">> 7"
    systemctl --user is-active pulseaudio
    echo ">> 8"
    pactl stat
    pulseaudio --check
    set +e
}

if [ "$1" = "malicious" ]; then
    # Lets copy the system pulseaudio apparmor profile and modify it to allow
    # loading the preloaded library
    TMP_DIR=$(mktemp -d)
    cp /etc/apparmor.d/usr.bin.pulseaudio ${TMP_DIR}
    rule="  ${TESTDIR}/${ARCHDIR}/lib/libpulseaudio-malicious-override.so rm,"
    rule=$(echo "$rule" | sed 's/\//\\\//g')
    sed -i ${TMP_DIR}/usr.bin.pulseaudio -e "s/\(profile \/usr\/bin\/pulseaudio {\)/\1\n${rule}/g"
    echo ">> Loading apparmor profile ${TMP_DIR}/usr.bin.pulseaudio"
    sudo apparmor_parser -r ${TMP_DIR}/usr.bin.pulseaudio

    # We set LD_BIND_NOW because if it's unset, pulseaudio re-execs itself
    # with it set, and the LD_PRELOAD env variable is lost.
    export LD_BIND_NOW=1
    export LD_PRELOAD="${TESTDIR}/${ARCHDIR}/lib/libpulseaudio-malicious-override.so"

    test_pulseaudio

    echo ">> Reloading apparmor profile /etc/apparmor.d/usr.bin.pulseaudio"
    sudo apparmor_parser -r /etc/apparmor.d/usr.bin.pulseaudio
else
    test_pulseaudio
fi
