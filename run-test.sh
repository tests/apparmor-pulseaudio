#!/bin/sh
# vim: tw=0

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

# We want to use the pre-existing session bus.
export LAUNCH_DBUS="no"

error=0
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/pulseaudio.normal.expected "${TESTDIR}"/pulseaudio normal || error=$?
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/pulseaudio.malicious.expected "${TESTDIR}"/pulseaudio malicious || error=$?

if [ $error = 0 ]; then
  echo "# apparmor-pulseaudio: all tests passed"
else
  echo "# apparmor-pulseaudio: at least one test failed"
fi

exit $error
